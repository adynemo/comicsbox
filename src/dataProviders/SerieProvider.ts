import baseURL from "@/config/baseURL";
import converter from "@/utils/objectConverter";
import Client from "@/plugins/client";
import Serie from "@/objects/Serie";
import Book from "@/objects/Book";
import imageHandler from "@/utils/imageHandler";

class SerieProvider {
	getOne(uid: string): Promise<Serie> {
		return new Promise((resolve, reject) => {
			Client.get(`${baseURL(true).series}/${uid}`)
				.then(response => {
					if (200 !== response.status) {
						reject(response.statusText);
					}
					return response.json();
				})
				.then(response => {
					const member = response['hydra:member'][0];
					const serie = new Serie();
					converter.convert(serie, member.serie, 'id');
					converter.convert(serie, member.serie, 'title');
					converter.convert(serie, member.serie, 'beginYear');
					converter.convert(serie, member.serie, 'endYear');
					converter.convert(serie, member.serie, 'createdAt');
					converter.convert(serie, member.serie, 'updatedAt');
					imageHandler.setCoverUrlFromMember(serie, member.serie);
					serie.publisher = member.serie.publisher.name;

					for (const data of member.books) {
						const book = new Book();
						converter.convert(book, data, 'id');
						converter.convert(book, data, 'title');
						converter.convert(book, data, 'slug');
						converter.convert(book, data, 'number');
						converter.convert(book, data, 'releaseDate');
						imageHandler.setCoverUrlFromMember(book, data);
						serie.addBook(book);
					}

					resolve(serie);
				})
				.catch(error => {
					reject(error);
				});
		})
	}
}

export default new SerieProvider();
