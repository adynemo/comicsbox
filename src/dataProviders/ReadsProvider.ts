import baseURL from "@/config/baseURL";
import Client from "@/plugins/client";
import Book from "@/objects/Book";

class ReadsProvider {
	post(user: any, book: Book): Promise<any> {
		return new Promise((resolve, reject) => {
			const readerRef = `${baseURL().users}/${user.id}`;
			const bookRef = `${baseURL().books}/${book.id}`;
			Client.post(baseURL().bookReads, {reader: readerRef, book: bookRef, count: 1})
				.then(response => {
					if (201 !== response.status) {
						reject(response.statusText);
					}
					return response.json();
				})
				.then(response => {
					const bookId = response.book.replace(`${baseURL().books}/`, '');
					const read = {
						id: response.id,
						book: bookId,
					};
					resolve(read);
				})
				.catch(error => {
					reject(error);
				});
		})
	}
	delete(uid: string): Promise<boolean> {
		return new Promise((resolve, reject) => {
			Client.delete(`${baseURL().bookReads}/${uid}`)
				.then(response => {
					if (204 !== response.status) {
						reject(response.statusText);
					}
					resolve(true);
				})
				.catch(error => {
					reject(error);
				});
		})
	}
	getByBooks(user: any, books: Array<Book>): Promise<any> {
		return new Promise((resolve, reject) => {
			const params = new URLSearchParams();
			params.append('reader', user.id);
			for (const book of books) {
				params.append('book[]', book.id);
			}
			Client.get(`${baseURL().bookReads}?${params.toString()}`)
				.then(response => {
					if (200 !== response.status) {
						reject(response.statusText);
					}
					return response.json();
				})
				.then(response => {
					const members = response['hydra:member'];
					const reads = [];
					for (const member of members) {
						const bookId = member.book.replace(`${baseURL().books}/`, '');
						reads.push({
							id: member.id,
							book: bookId,
						});
					}
					resolve(reads);
				})
				.catch(error => {
					reject(error);
				});
		})
	}
}

export default new ReadsProvider();
