import baseURL from "@/config/baseURL";
import converter from "@/utils/objectConverter";
import Client from "@/plugins/client";
import Book from "@/objects/Book";
import Character from "@/objects/Character";
import Serie from "@/objects/Serie";
import imageHandler from "@/utils/imageHandler";
import {SearchProviderInterface} from "@/dataProviders/ProviderInterfaces";

class CharacterProvider implements SearchProviderInterface {
	getOne(uid: string): Promise<Character> {
		return new Promise((resolve, reject) => {
			Client.get(`${baseURL(true).characters}/${uid}`)
				.then(response => {
					if (200 !== response.status) {
						reject(response.statusText);
					}
					return response.json();
				})
				.then(response => {
					const member = response['hydra:member'][0];
					const character = new Character();
					converter.convert(character, member.character, 'id');
					converter.convert(character, member.character, 'name');
					converter.convert(character, member.character, 'presentation');
					converter.convert(character, member.character, 'creationYear');
					converter.convert(character, member.character, 'createdAt');
					converter.convert(character, member.character, 'updatedAt');
					imageHandler.setAvatarUrlFromMember(character, member.character);

					const firstAppearance = member.firstAppearance;
					if (0 < Object.keys(firstAppearance).length) {
						const book = new Book();
						converter.convert(book, firstAppearance, 'id');
						converter.convert(book, firstAppearance, 'number');
						imageHandler.setCoverUrlFromMember(book, firstAppearance);
						character.firstAppearance = book;
					}

					for (const data of member.series) {
						const serie = new Serie();
						converter.convert(serie, data, 'id');
						converter.convert(serie, data, 'title');
						converter.convert(serie, data, 'beginYear');
						converter.convert(serie, data, 'endYear');
						imageHandler.setCoverUrlFromMember(serie, data);
						character.addSerie(serie);
					}

					resolve(character);
				})
				.catch(error => {
					reject(error);
				});
		})
	}
	search(query: {name?: string}): Promise<Character[]|null> {
		return new Promise((resolve, reject) => {
				let doQuery = false;
				let uri = `${baseURL().characters}?`;
				if (Object.prototype.hasOwnProperty.call(query, 'name') && '' !== query.name) {
					uri = `${uri}name=${query.name}`;
					doQuery = true;
				}

				if (!doQuery) {
					resolve(null);
				}

				Client.get(uri)
					.then(response => {
						if (200 !== response.status) {
							reject(response.statusText);
						}
						return response.json();
					})
					.then(response => {
						const members = response['hydra:member'];
						const characters: Character[] = [];
						for (const member of members) {
							const character = new Character();
							converter.convert(character, member, 'id');
							converter.convert(character, member, 'name');
							imageHandler.setAvatarUrlFromMember(character, member);

							characters.push(character);
						}

						resolve(characters);
					})
					.catch(error => {
						reject(error);
					});
			}
		)
	}
}

export default new CharacterProvider();
