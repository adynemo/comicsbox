import Vue from 'vue'
import VueRouter from 'vue-router'
import {RouteConfig} from "vue-router";
import HomeView from "@/views/HomeView.vue";
import LoginView from "@/views/LoginView.vue";
import RegisterView from "@/views/RegisterView.vue";
import ProfileView from "@/views/ProfileView.vue";
import BookView from "@/views/BookView.vue";
import SerieView from "@/views/SerieView.vue";
import CharacterView from "@/views/CharacterView.vue";
import AuthorView from "@/views/AuthorView.vue";
import AdminHome from "@/views/admin/AdminHome.vue";
import CreateBookView from "@/views/admin/CreateBookView.vue";

Vue.use(VueRouter)

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: {
      requiresAuth: false,
      icon: 'mdi-home'
    },
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView,
    meta: {
      requiresAuth: null,
      icon: 'mdi-login-variant'
    },
  },
  {
    path: '/register',
    name: 'register',
    component: RegisterView,
    meta: {
      requiresAuth: null,
      icon: 'mdi-account-plus-outline'
    },
  },
  {
    path: '/profile',
    name: 'profile',
    component: ProfileView,
    meta: {
      requiresAuth: true,
      icon: 'mdi-account'
    },
  },
  {
    path: '/comics/book/:id',
    name: 'book',
    component: BookView,
    meta: {
      requiresAuth: null,
      icon: null
    },
  },
  {
    path: '/comics/serie/:id',
    name: 'serie',
    component: SerieView,
    meta: {
      requiresAuth: null,
      icon: null
    },
  },
  {
    path: '/characters/:id',
    name: 'character',
    component: CharacterView,
    meta: {
      requiresAuth: null,
      icon: null
    },
  },
  {
    path: '/authors/:id',
    name: 'author',
    component: AuthorView,
    meta: {
      requiresAuth: null,
      icon: null
    },
  },
  {
    path: '/admin',
    name: 'Admin Home',
    component: AdminHome,
    meta: {
      requiresAuth: true,
      icon: 'mdi-cog'
    },
    children: [
      {
        path: 'book/create',
        name: 'Create Book',
        component: CreateBookView,
        meta: {
          requiresAuth: null,
          icon: null
        },
      }
    ]
  }
]

const router = new VueRouter({
  mode: process.env.IS_ELECTRON ? 'hash' : 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
