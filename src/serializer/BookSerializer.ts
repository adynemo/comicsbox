import Format from "@/objects/Format";
import baseURL from "@/config/baseURL";
import Country from "@/objects/Country";
import Serie from "@/objects/Serie";
import Book, {BookSerializable} from "@/objects/Book";
import Author from "@/objects/Author";
import Character from "@/objects/Character";
import strings from "@/utils/strings";

const BookSerializer = {
    clean(book: BookSerializable): BookSerializable {
        if (book.format && book.format instanceof Format) {
            book.format = `${baseURL().formats}/${book.format.id}`;
        }
        if (book.country && book.country instanceof Country) {
            book.country = `${baseURL().countries}/${book.country.id}`;
        }
        if (book.serie && book.serie instanceof Serie) {
            book.serie = `${baseURL().series}/${book.serie.id}`;
        }
        if (book.originalBook && book.originalBook instanceof Book) {
            book.originalBook = `${baseURL().books}/${book.originalBook.id}`;
        }

        if (book.authors) {
            const authors = [];
            for (const author of book.authors) {
                if (!(author instanceof Author)) continue
                const roles = [];
                for (const role of author.roles) {
                    roles.push(`${baseURL().authorRoles}/${role.id}`);
                }
                authors.push({
                    author: `${baseURL().authors}/${author.id}`,
                    roles: roles,
                });
            }

            if (0 < authors.length) {
                book.authors = authors;
            }
        }

        if (book.characters) {
            const characters = [];
            for (const character of book.characters) {
                let uid;
                if (!(character instanceof Character)) {
                    if (!strings.isUuid(character)) continue;
                    uid = character;
                } else {
                    if (!strings.isUuid(character.id as string)) continue;
                    uid = character.id;
                }
                characters.push(`${baseURL().characters}/${uid}`);
            }

            if (0 < characters.length) {
                book.characters = characters;
            } else {
                delete book.characters;
            }
        }

        if (book.issues) {
            const issues = [];
            for (const issue of book.issues) {
                let uid;
                if (!(issue instanceof Book)) {
                    if (!strings.isUuid(issue)) continue;
                    uid = issue;
                } else {
                    if (!strings.isUuid(issue.id)) continue;
                    uid = issue.id;
                }
                issues.push(`${baseURL().books}/${uid}`);
            }

            if (0 < issues.length) {
                book.issues = issues;
            } else {
                delete book.issues;
            }
        }

        return book;
    }
}

export default BookSerializer;
