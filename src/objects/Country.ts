class Country {
    public id: string;
    public name: string;
    public code: string;
    public currency: string;
    constructor() {
        this.id = '';
        this.name = '';
        this.code = '';
        this.currency = '';
    }
}

export default Country;
