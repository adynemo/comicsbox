import {Route} from "vue-router/types/router";
import strings from "@/utils/strings";

export const requestHelper = {
	generateParams: (options: any): string => {
		let params = '';
		for (const key in options) {
			if (typeof options[key] === 'object') {
				const subOpt = options[key];
				for (const k in options[key]) {
					params += `${key}[${k}]=${subOpt[k]}&`;
				}
				continue;
			}

			params += `${key}=${options[key]}&`;
		}

		return params.slice(0, -1);
	},
	getUuidFromQuery(route: Route, param: string): undefined|string {
		if (
			Object.prototype.hasOwnProperty.call(route.query, param)
			&& typeof route.query[param] === 'string'
			// @ts-ignore
			&& strings.isUuid(route.query[param])
		) {
			// @ts-ignore
			return route.query[param];
		}
	}
}
