const strings = {
	capitalize: (value: string): string => {
		if (!value) return '';
		value = value.toString();
		return value.charAt(0).toUpperCase() + value.slice(1);
	},
	formatDate: (
		dateValue: string|null,
		local = 'en-US',
		option = { year: 'numeric', month: 'long', day: 'numeric', timeZone: 'UTC' }
	): string|void => {
		if (dateValue) {
			const theDate = new Date(dateValue);
			return theDate.toLocaleDateString(local, option);
		}
	},
	generateUniqueId(): string {
		return Math.random().toString(36).substr(2, 9);
	},
	isUuid(uuid: string): boolean {
		return /^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$/.test(uuid);
	},
	isIsbn(isbn: string): boolean {
		return /^(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/.test(isbn);
	}
}

export default strings;
