import baseURL from "@/config/baseURL";
import converter from "@/utils/objectConverter";
import Client from "@/plugins/client";
import Format from "@/objects/Format";

class FormatProvider {
	getAll(): Promise<Format[]> {
		return new Promise((resolve, reject) => {
				Client.get(`${baseURL().formats}`)
					.then(response => {
						if (200 !== response.status) {
							reject(response.statusText);
						}
						return response.json();
					})
					.then(response => {
						const members = response['hydra:member'];
						const formats: Format[] = [];
						for (const member of members) {
							const format = new Format();
							converter.convert(format, member, 'id');
							converter.convert(format, member, 'label');

							formats.push(format);
						}

						resolve(formats)
					})
					.catch(error => {
						reject(error);
					});
			}
		)
	}
}

export default new FormatProvider();
