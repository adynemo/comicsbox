import client from "@/plugins/client";
import decodeJwt from 'jwt-decode';
import baseURL from "@/config/baseURL";
import {User, UserLogin, tokenName, UserLogged} from "@/objects/User";

interface LoginResult {
    success: boolean,
    token: string
}

interface AuthState {
    isUserLoggedIn: boolean,
    isProcessing: boolean,
    loginToken: string,
}

const authStore = {
    state: {
        isUserLoggedIn: false,
        isProcessing: false,
        loginToken: '',
    },
    mutations: {
        setLogged(state: AuthState, loginResult: LoginResult) {
            state.isUserLoggedIn = loginResult.success;
            state.loginToken = loginResult.token;
            if (loginResult.success) {
                sessionStorage.setItem(tokenName, loginResult.token)
            } else {
                sessionStorage.removeItem(tokenName);
            }
        },
        setIsProcessing(state: AuthState, isProcessing: boolean) {
            state.isProcessing = isProcessing
        },
        setLogout(state: AuthState) {
            sessionStorage.removeItem(tokenName);
            state.isUserLoggedIn = false;
        },
    },
    actions: {
        registerNewUser({ commit, dispatch }: any, payload: User): Promise<Response> {
            return new Promise((resolve, reject) => {
                commit('setIsProcessing', true);
                client.post(
                    baseURL().users,
                    {username: payload.username, password: payload.password, email: payload.email}
                )
                    .then(response => {
                        if (201 !== response.status) {
                            throw new Error(response.statusText);
                        }
                        return response.json();
                    })
                    .then(response => {
                        commit('setIsProcessing', false);
                        commit('setRegistrationStatus', {success: true});
                        dispatch('displaySnackbar', { success: true, message: "You're now a ComicsBoxer!" });
                        resolve(response)
                    })
                    .catch(error => {
                        commit('setIsProcessing', false);
                        if (typeof error != 'undefined' && typeof error.response != 'undefined') {
                            commit('setRegistrationStatus', {success: false});
                            dispatch('displaySnackbar', {success: false, message: error.response.data.message});
                        } else {
                            commit('setRegistrationStatus', {success: false});
                            dispatch('displaySnackbar', {success: false, message: error.message});
                        }
                        reject()
                    });
            });
        },
        authenticateUserAndSetToken({ commit, dispatch }: any, payload: UserLogin): Promise<boolean> {
            return new Promise((resolve, reject) => {
                commit('setIsProcessing', true);
                client.post(baseURL().token, payload)
                    .then(response => {
                        if (403 === response.status || 401 === response.status) {
                            throw new Error(`You don't have permissions`);
                        }
                        if (response.status < 200 || response.status >= 300) {
                            throw new Error(response.statusText);
                        }
                        return response.json();
                    })
                    .then(auth => {
                        commit('setIsProcessing', false);
                        commit('setLogged', { success: true, token: auth.token });
                        dispatch('displaySnackbar', { success: true, message: `Welcome ${payload.username}!` });
                        resolve(true);
                    })
                    .catch(error => {
                        commit('setIsProcessing', false);
                        if (typeof error != 'undefined' && typeof error.response != 'undefined') {
                            commit('setLogged', { success: false });
                            dispatch('displaySnackbar', { success: false, message: error.response.data.message });
                        } else {
                            commit('setLogged', { success: false });
                            dispatch('displaySnackbar', { success: false, message: error.message });
                        }
                        reject();
                    });
            });

        },
        logout({ commit, dispatch }: any) {
            commit('setIsProcessing', false);
            commit('setLogout');
            dispatch('displaySnackbar', { success: true, message: "You have been disconnected" });
        },
        checkSession({ commit }: any) {
            const loginToken: string|null = sessionStorage.getItem(tokenName);
            if (null !== loginToken) {
                // @ts-ignore
                const expirationDate: Date = new Date(decodeJwt(loginToken).exp);
                if (((new Date()).getTime() / 1000) > expirationDate.getTime()) {
                    commit('setLogout')
                }
            }
        },
    },
    getters: {
        isProcessing(state: AuthState) {
            return state.isProcessing;
        },
        isUserLoggedIn(state: AuthState) {
            if (!state.isUserLoggedIn) {
                state.isUserLoggedIn = !!sessionStorage.getItem(tokenName);
                return state.isUserLoggedIn;
            }
            return true;
        },
        user(): UserLogged|void {
            const token = sessionStorage.getItem(tokenName);
            if (token) {
                return decodeJwt(token);
            }
        },
    }
}

export default authStore;
