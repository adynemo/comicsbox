FROM node:latest

RUN apt update && apt install -y sudo

RUN yarn global add @api-platform/client-generator @vue/cli

USER node

WORKDIR /app

EXPOSE 3000
