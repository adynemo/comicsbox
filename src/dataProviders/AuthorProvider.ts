import baseURL from "@/config/baseURL";
import converter from "@/utils/objectConverter";
import Client from "@/plugins/client";
import Author from "@/objects/Author";
import Serie from "@/objects/Serie";
import imageHandler from "@/utils/imageHandler";
import {SearchProviderInterface} from "@/dataProviders/ProviderInterfaces";

class AuthorProvider implements SearchProviderInterface {
	getOne(uid: string): Promise<Author> {
		return new Promise((resolve, reject) => {
			Client.get(`${baseURL(true).authors}/${uid}`)
				.then(response => {
					if (200 !== response.status) {
						reject(response.statusText);
					}
					return response.json();
				})
				.then(response => {
					const member = response['hydra:member'][0];
					const author = new Author();
					converter.convert(author, member.author, 'id');
					converter.convert(author, member.author, 'name');
					converter.convert(author, member.author, 'presentation');
					converter.convert(author, member.author, 'createdAt');
					converter.convert(author, member.author, 'updatedAt');
					imageHandler.setAvatarUrlFromMember(author, member.author);

					for (const data of member.series) {
						const serie = new Serie();
						converter.convert(serie, data, 'id');
						converter.convert(serie, data, 'title');
						converter.convert(serie, data, 'beginYear');
						converter.convert(serie, data, 'endYear');
						imageHandler.setCoverUrlFromMember(serie, data);
						author.addSerie(serie);
					}

					resolve(author);
				})
		})
	}
	getAll(): Promise<Author[]> {
		return new Promise((resolve, reject) => {
				Client.get(`${baseURL().authors}`)
					.then(response => {
						if (200 !== response.status) {
							reject(response.statusText);
						}
						return response.json();
					})
					.then(response => {
						const members = response['hydra:member'];
						const authors: Author[] = [];
						for (const member of members) {
							const author = new Author();
							converter.convert(author, member, 'id');
							converter.convert(author, member, 'name');

							authors.push(author);
						}

						resolve(authors)
					})
			}
		)
	}
	search(query: {name?: string}): Promise<Author[]|null> {
		return new Promise((resolve, reject) => {
			let doQuery = false;
			let uri = `${baseURL().authors}?`;
			if (Object.prototype.hasOwnProperty.call(query, 'name') && '' !== query.name) {
				uri = `${uri}name=${query.name}`;
				doQuery = true;
			}

			if (!doQuery) {
				resolve(null);
			}

			Client.get(uri)
				.then(response => {
					if (200 !== response.status) {
						reject(response.statusText);
					}
					return response.json();
				})
				.then(response => {
					const members = response['hydra:member'];
					const authors: Author[] = [];
					for (const member of members) {
						const author = new Author();
						converter.convert(author, member, 'id');
						converter.convert(author, member, 'name');
						imageHandler.setAvatarUrlFromMember(author, member);

						authors.push(author);
					}

					resolve(authors);
				})
				.catch(error => {
					reject(error);
				});
			}
		)
	}
}

export default new AuthorProvider();
