import Vue from 'vue'
import App from '@/App.vue'
import vuetify from '@/plugins/vuetify'
import router from '@/router'
import store from '@/store'
import strings from "@/utils/strings";

Vue.config.productionTip = false

Vue.filter('capitalize', strings.capitalize)

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
