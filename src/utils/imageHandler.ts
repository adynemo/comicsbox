const entrypoint: string = process.env.VUE_APP_COMICSBOX_ENTRYPOINT || '';

const imageHandler = {
  setCoverUrlFromMember: (object: any, member: any): void => {
    let cover = '/no-cover-med.jpg';
    if (member.image && member.image.url) {
      cover = entrypoint + member.image.url;
    }

    object.cover = cover;
  },
  setAvatarUrlFromMember: (object: any, member: any): void => {
    let avatar = '/default-avatar.png';
    if (member.image?.url) {
      avatar = entrypoint + member.image.url;
    }

    object.avatar = avatar;
  }
}

export default imageHandler;
