import Vue from 'vue';
import Vuetify from 'vuetify';
// import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: true,
        themes: {
            dark: {
                primary: '#4b0815',
                secondary: '#FFE4C4'
            }
        }
    },
});
