import baseURL from "@/config/baseURL";
import converter from "@/utils/objectConverter";
import Client from "@/plugins/client";
import AuthorRole from "@/objects/AuthorRole";

class AuthorRoleProvider {
	getAll(): Promise<AuthorRole[]> {
		return new Promise((resolve, reject) => {
				Client.get(`${baseURL().authorRoles}`)
					.then(response => {
						if (200 !== response.status) {
							reject(response.statusText);
						}
						return response.json();
					})
					.then(response => {
						const members = response['hydra:member'];
						const roles: AuthorRole[] = [];
						for (const member of members) {
							const role = new AuthorRole();
							converter.convert(role, member, 'id');
							converter.convert(role, member, 'name');

							roles.push(role);
						}

						resolve(roles)
					})
					.catch(error => {
						reject(error);
					});
			}
		)
	}
}

export default new AuthorRoleProvider();
