import {tokenName} from "@/objects/User";
import {ENTRYPOINT as entrypoint} from "@/config/entrypoint";

class Client {
    post(uri: string, body: any) {
        if (!uri || !body) {
            throw new Error('You must provide uri and body to post a request');
        }

        const headers = this.getHeaders();
        const options = {
            method: 'POST',
            body: '' as string|FormData,
            headers: headers,
        };

        if (body instanceof File) {
            const data = new FormData();
            data.append('file', body);
            options.headers.delete('Content-Type');
            options.body = data;
        } else {
            options.body = JSON.stringify(body);
        }

        const request = new Request(`${entrypoint}${uri}`, options);

        return fetch(request);
    }
    get(uri: string) {
        if (!uri) {
            throw new Error('You must provide uri to post a request');
        }

        const headers = this.getHeaders();
        const request = new Request(`${entrypoint}${uri}`, {
            method: 'GET',
            headers: headers,
        });

        return fetch(request);
    }
    delete(uri: string) {
        if (!uri) {
            throw new Error('You must provide uri to post a request');
        }

        const headers = this.getHeaders();
        const request = new Request(`${entrypoint}${uri}`, {
            method: 'DELETE',
            headers: headers,
        });

        return fetch(request);
    }
    getHeaders() {
        const headers = new Headers({'Content-Type': 'application/json'});
        const token = sessionStorage.getItem(tokenName);
        if (null !== token) {
            headers.set('Authorization', `Bearer ${token}`);
        }

        return headers;
    }
}

export default new Client();
