import baseURL from "@/config/baseURL";
import converter from "@/utils/objectConverter";
import Client from "@/plugins/client";
import Country from "@/objects/Country";

class CountryProvider {
	getAll(): Promise<Country[]> {
		return new Promise((resolve, reject) => {
				Client.get(`${baseURL().countries}`)
					.then(response => {
						if (200 !== response.status) {
							reject(response.statusText);
						}
						return response.json();
					})
					.then(response => {
						const members = response['hydra:member'];
						const countries: Country[] = [];
						for (const member of members) {
							const country = new Country();
							converter.convert(country, member, 'id');
							converter.convert(country, member, 'name');
							converter.convert(country, member, 'code');
							converter.convert(country, member, 'currency');

							countries.push(country);
						}

						resolve(countries)
					})
					.catch(error => {
						reject(error);
					});
			}
		)
	}
}

export default new CountryProvider();
