import Author from "@/objects/Author";
import baseURL from "@/config/baseURL";
import converter from "@/utils/objectConverter";
import Client from "@/plugins/client";
import Book, {BookSerializable} from "@/objects/Book";
import {requestHelper} from "@/utils/requestHelper";
import Serie from "@/objects/Serie";
import Character from "@/objects/Character";
import imageHandler from "@/utils/imageHandler";
import ImageProvider from "@/dataProviders/ImageProvider";
import BookSerializer from "@/serializer/BookSerializer";

interface BookCard {
	previous: Book,
	current: Book,
	next: Book,
	read: any
}

const postBook = (uri: string, body: any) => {
	return new Promise((resolve, reject) => {
		return Client.post(uri, body)
			.then(response => {
				if (201 !== response.status) {
					reject(response.statusText);
				}
				return response.json();
			})
			.then(response => {
				resolve(response)
			});
	});
}

// todo: better conversion in BookProvider for post
class BookProvider {
	getOne(uid: string): Promise<BookCard> {
		return new Promise((resolve, reject) => {
			Client.get(`${baseURL(true).books}/${uid}`)
				.then(response => {
					if (200 !== response.status) {
						reject(response.statusText);
					}
					return response.json();
				})
				.then(response => {
					const member = response['hydra:member'][0];
					const current = member.current;
					const book = new Book();
					converter.convert(book, current, 'id');
					converter.convert(book, current, 'slug');
					converter.convert(book, current, 'title');
					converter.convert(book, current, 'description');
					converter.convert(book, current, 'number');
					converter.convert(book, current, 'pageCount');
					converter.convert(book, current, 'createdAt');
					converter.convert(book, current, 'updatedAt');
					converter.convert(book, current, 'releaseDate');
					converter.convert(book, current, 'price');
					converter.convert(book, current, 'sku');
					converter.convert(book, current, 'isbn');
					converter.convert(book, current, 'country');
					imageHandler.setCoverUrlFromMember(book, current);

					if (current.serie) {
						const serie = new Serie();
						converter.convert(serie, current.serie, 'id');
						converter.convert(serie, current.serie, 'title');
						book.serie = serie;
					}

					for (const data of current.authors) {
						const author = new Author();
						converter.convert(author, data.author, 'id');
						converter.convert(author, data.author, 'name');
						imageHandler.setAvatarUrlFromMember(author, data.author);
						data.roles.map((role: any) => {
							author.addRole(role.name);
						});
						book.addAuthor(author);
					}

					for (const data of current.characters) {
						const character = new Character();
						converter.convert(character, data, 'id');
						converter.convert(character, data, 'name');
						imageHandler.setAvatarUrlFromMember(character, data);
						book.addCharacter(character);
					}

					resolve({
						previous: member.previous,
						current: book,
						next: member.next,
						read: member.read,
					});
				})
				.catch(error => {
					reject(error);
				});
		})
	}
	getAll(options: any = null): Promise<Book[]> {
		return new Promise((resolve, reject) => {
			let uri = `${baseURL().books}`;
			if (typeof options === 'object') {
				const params = requestHelper.generateParams(options);
				uri += `?${params}`;
			}
			Client.get(uri)
				.then(response => {
					if (200 !== response.status) {
						reject(response.statusText);
					}
					return response.json();
				})
				.then(response => {
					const members = response['hydra:member'];
					const books: Book[] = [];
					for (const member of members) {
						const book = new Book();
						converter.convert(book, member, 'id');
						converter.convert(book, member, 'title');
						converter.convert(book, member, 'slug');
						converter.convert(book, member, 'number');
						converter.convert(book, member, 'releaseDate');
						imageHandler.setCoverUrlFromMember(book, member);

						if (member.serie) {
							const serie = new Serie();
							converter.convert(serie, member.serie, 'id');
							converter.convert(serie, member.serie, 'title');
							book.serie = serie;
						}

						books.push(book);
					}

					resolve(books)
				})
				.catch(error => {
					reject(error);
				});
			}
		)
	}
	post(book: BookSerializable, cover?: File): Promise<any> {
		return new Promise((resolve, reject) => {
			const uri = `${baseURL().books}`;
			book = BookSerializer.clean(book);

			if (cover) {
				// process cover
				ImageProvider.postCover(cover)
					.then(image => {
						book.image = `${baseURL().covers}/${image.id}`;
						postBook(uri, book).then(response => resolve(response)).catch(error => reject(error));
					})
					.catch(error => reject(error))
			} else {
				postBook(uri, book).then(response => resolve(response)).catch(error => reject(error));
			}
		})
	}
}

export default new BookProvider();
