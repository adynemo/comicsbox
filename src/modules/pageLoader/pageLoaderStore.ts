interface PageLoaderState {
    enabled: boolean
}

const store = {
    state: {
        enabled: false,
    },
    mutations: {
        togglePageLoader(state: PageLoaderState, enabled: boolean) {
            state.enabled = enabled;
        },
    },
    actions: {
        togglePageLoader({commit}: any, enabled: boolean) {
            commit('togglePageLoader', enabled);
        },
    },
    getters: {
        isPageLoaderEnabled(state: PageLoaderState) {
            return state.enabled;
        },
    }
}

export default store;
