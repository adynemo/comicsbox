import Serie from "./Serie";
import Author, {AuthorSerializableType} from "./Author";
import Character, {CharacterSerializableType} from "./Character";
import Country from "@/objects/Country";
import Format from "@/objects/Format";
import Image from "@/objects/Image";

// todo: rename image properties from all object by `image`
// todo: maybe add an identifier like name or title for all objects
class Book {
	public id: string;
	public cover: string;
	public description: string;
	public number: number;
	public createdAt: string|null;
	public updatedAt: string|null;
	public releaseDate: string|null;
	public serie: Serie;
	public title: string;
	public pageCount: number;
	public price: number|null;
	public sku: string|null;
	public isbn: string|null;
	public country: Country;
	public format: Format;
	public readonly authors: Author[];
	public readonly characters: Character[];
	public originalBook: Book|undefined;
	public issues: Book[];
	public read: string|null;
	public slug: string;
	constructor() {
		this.id = '';
		this.cover = '';
		this.description = '';
		this.number = 0;
		this.createdAt = null;
		this.updatedAt = null;
		this.releaseDate = null;
		this.serie = new Serie();
		this.title = '';
		this.pageCount = 0;
		this.authors = [];
		this.characters = [];
		this.issues = [];
		this.price = null;
		this.sku = null;
		this.isbn = null;
		this.country = new Country();
		this.format = new Format();
		this.read = null;
		this.slug = '';
	}
	addAuthor(author: Author) {
		this.authors.push(author);
	}
	addCharacter(character: Character) {
		this.characters.push(character);
	}
	addIssue(issue: Book) {
		this.issues.push(issue);
	}
}

export default Book;

export type BookSerializableType = string|Book;

class BookSerializable {
	public id?: string;
	public description?: string;
	public number?: number;
	public releaseDate?: string;
	public title?: string;
	public pageCount?: number;
	public price?: number;
	public sku?: string;
	public isbn?: string;
	public image?: string|Image;
	public serie?: string|Serie;
	public country?: string|Country;
	public format?: string|Format;
	public authors?: AuthorSerializableType[];
	public characters?: CharacterSerializableType[];
	public originalBook?: string|Book;
	public issues?: BookSerializableType[];
	initializeForCreation() {
		this.country = new Country();
		this.format = new Format();
	}
	addAuthor(author: AuthorSerializableType) {
		if (!this.authors) {
			this.authors = [];
		}
		this.authors.push(author);
	}
	addCharacter(character: CharacterSerializableType) {
		if (!this.characters) {
			this.characters = [];
		}
		this.characters.push(character);
	}
	addIssue(issue: BookSerializableType) {
		if (!this.issues) {
			this.issues = [];
		}
		this.issues.push(issue);
	}
}

export {BookSerializable};
