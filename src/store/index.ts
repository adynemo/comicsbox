import Vue from 'vue'
import Vuex from 'vuex'
import authStore from '@/modules/authentication/authStore'
import notificationStore from '@/modules/notification/notificationStore'
import currentPageStore from "@/modules/currentPage/currentPageStore";
import pageLoaderStore from "@/modules/pageLoader/pageLoaderStore";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    authStore,
    notificationStore,
    currentPageStore,
    pageLoaderStore
  }
})
