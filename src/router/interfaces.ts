import {Component} from "vue";

export interface Route {
    name: string,
    params: object
}
