interface CurrentPageState {
    currentPageName: string|boolean,
    banner: string|null,
}
const store = {
    state: {
        currentPageName: '',
        banner: null,
    },
    mutations: {
        setCurrentPageName(state: CurrentPageState, name: string) {
            state.currentPageName = name;
        },
        setBanner(state: CurrentPageState, url: string) {
            state.banner = url;
        },
    },
    actions: {
        setCurrentPageName({commit}: any, name: string) {
            commit('setCurrentPageName', name);
        },
        setBanner({commit}: any, url: string) {
            commit('setBanner', url);
        },
        resetBanner({commit}: any) {
            commit('setBanner', null);
        },
    },
    getters: {
        getCurrentPageName(state: CurrentPageState) {
            return state.currentPageName;
        },
        getBanner(state: CurrentPageState) {
            return state.banner;
        }
    }
}

export default store;
