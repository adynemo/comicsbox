export interface UserLogin {
    username: string,
    password: string,
}

export interface User extends UserLogin {
    email: string,
}

export interface UserLogged {
    id: string,
    username: string,
    email: string,
    avatar: string|null,
    exp: number,
    iat: number,
    roles: string[],
}

export const tokenName: string = 'loginToken';
