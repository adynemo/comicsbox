import Types from "@/objects/Types";
import AuthorProvider from "@/dataProviders/AuthorProvider";
import SerieProvider from "@/dataProviders/SerieProvider";
import CharacterProvider from "@/dataProviders/CharacterProvider";

class ProviderFactory {
    getProvider(type: string) {
        if (Types.author === type) {
            return AuthorProvider;
        }
        if (Types.serie === type) {
            return SerieProvider;
        }
        if (Types.character === type) {
            return CharacterProvider;
        }

        throw new Error('Unable to load provider');
    }
}

export default new ProviderFactory();
