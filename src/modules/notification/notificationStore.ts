interface NotificationState {
    isSnackbarVisible: boolean,
    message: string,
    success: boolean,
}

const store = {
    state: {
        isSnackbarVisible: false,
        message: '',
        success: false,
    },
    mutations: {
        setIsSnackbarVisible(state: NotificationState, isVisible: boolean) {
            state.isSnackbarVisible = isVisible;
        },
        setSnackbarMessage(state: NotificationState, message: string) {
            state.message = message;
        },
        setSnackbarSuccess(state: NotificationState, success: boolean) {
            state.success = success;
        },
    },
    actions: {
        setSnackbarVisibility({commit}: any, isVisible: boolean) {
            commit('setIsSnackbarVisible', isVisible);
        },
        displaySnackbar({commit}: any, payload: Partial<NotificationState>) {
            commit('setIsSnackbarVisible', true);
            commit('setSnackbarMessage', payload.message);
            commit('setSnackbarSuccess', payload.success);
        }
    },
    getters: {
        isSnackbarVisible(state: NotificationState) {
            return state.isSnackbarVisible;
        },
        getSnackbarMessage(state: NotificationState) {
            return state.message;
        },
        isSnackbarSuccess(state: NotificationState) {
            return state.success;
        },
    }
}

export default store;
