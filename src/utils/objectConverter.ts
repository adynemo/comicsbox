const converter = {
	convert: (object: any, filler: any, key: string): void => {
		if (Object.prototype.hasOwnProperty.call(object, key)) {
			object[key] = filler[key];
		}
	},
	fill: (object: any, key: string, value: any): void => {
		if (Object.prototype.hasOwnProperty.call(object, key)) {
			object[key] = value;
		}
	}
}

export default converter;
