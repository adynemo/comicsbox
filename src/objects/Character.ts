import Book from "./Book";
import Serie from "./Serie";

class Character {
	public id: string|null;
	public name: string|null;
	public avatar: string|null;
	public presentation: string|null;
	public creationYear: number|null;
	public firstAppearance: Book|null;
	public readonly series: Serie[];
	public createdAt: string|null;
	public updatedAt: string|null;
	constructor() {
		this.id = null;
		this.name = null;
		this.avatar = null;
		this.presentation = null;
		this.creationYear = null;
		this.firstAppearance = null;
		this.series = [];
		this.createdAt = null;
		this.updatedAt = null;
	}
	addSerie(serie: Serie) {
		this.series.push(serie);
	}
}

export default Character;

export type CharacterSerializableType = string|Character;
