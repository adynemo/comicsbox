import baseURL from "@/config/baseURL";
import converter from "@/utils/objectConverter";
import Client from "@/plugins/client";
import Image from "@/objects/Image";

const post = (image: File, url: string): Promise<Image> => {
	return new Promise((resolve, reject) => {
			Client.post(url, image)
				.then(response => {
					if (201 !== response.status) {
						reject(response.statusText);
					}
					return response.json();
				})
				.then(response => {
					const image = new Image();
					converter.convert(image, response, 'id');
					converter.convert(image, response, 'url');

					resolve(image);
				})
				.catch(error => {
					reject(error);
				});
		}
	)
};

class ImageProvider {
	postCover(image: File): Promise<Image> {
		const url = baseURL().covers;
		return post(image, url);
	}
}

export default new ImageProvider();
