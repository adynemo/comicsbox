const baseAPI = '/api';
const baseExtendedAPI = `${baseAPI}/proxy`;

const baseURL = (extended = false) => {
	const prefix = extended ? baseExtendedAPI : baseAPI;
	return {
		authors: `${prefix}/authors`,
		authorRoles: `${prefix}/author_roles`,
		books: `${prefix}/books`,
		bookReads: `${prefix}/book_reads`,
		characters: `${prefix}/characters`,
		countries: `${prefix}/countries`,
		covers: `${prefix}/covers`,
		formats: `${prefix}/formats`,
		series: `${prefix}/series`,
		token: '/authentication_token',
		users: `${prefix}/users`
	}
};

export default baseURL;
