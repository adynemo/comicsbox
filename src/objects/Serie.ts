import Book from "./Book";

class Serie {
	public id: string;
	public title: string;
	public beginYear: number|null;
	public endYear: number|null;
	public readonly books: Book[];
	public publisher: string;
	public createdAt: string|null;
	public updatedAt: string|null;
	public cover: string;
	constructor() {
		this.id = '';
		this.title = '';
		this.beginYear = null;
		this.endYear = null;
		this.books = [];
		this.publisher = '';
		this.createdAt = null;
		this.updatedAt = null;
		this.cover = '';
	}
	addBook(book: Book) {
		this.books.push(book);
	}
}

export default Serie;
