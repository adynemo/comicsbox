import Serie from "@/objects/Serie";
import AuthorRole from "@/objects/AuthorRole";

class Author {
	public id: string|null;
	public readonly roles: AuthorRole[];
	public readonly series: Serie[];
	public name: null;
	public avatar: null;
	public presentation: null;
	public createdAt: null;
	public updatedAt: null;
	constructor() {
		this.id = null;
		this.roles = [];
		this.series = [];
		this.name = null;
		this.avatar = null;
		this.presentation = null;
		this.createdAt = null;
		this.updatedAt = null;
	}
	addRole(role: AuthorRole) {
		if (-1 !== this.roles.findIndex(authorRole => authorRole.id === role.id)) return;
		this.roles.push(role);
	}
	removeRole(id: string) {
		const i = this.roles.findIndex(role => role.id === id);
		this.roles.splice(i, i >= 0 ? 1 : 0);
	}
	addSerie(serie: Serie) {
		this.series.push(serie);
	}
}

export default Author;

export type AuthorRelation = {author: string, roles: string[]};
export type AuthorSerializableType = AuthorRelation|Author;
