export interface SearchProviderInterface {
    search(query: Record<string,any>): Promise<any[]|null>
}
