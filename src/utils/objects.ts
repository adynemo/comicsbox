const objects = {
    alphabeticSort(a: any, b: any, field: string): number {
        if (!Object.prototype.hasOwnProperty.call(a, field) || !Object.prototype.hasOwnProperty.call(b, field)) {
            throw new Error('Unable to sort. This object has not the field parameter as property');
        }
        if (a[field] < b[field]) {
            return -1;
        }
        if (a[field] > b[field]) {
            return 1;
        }
        return 0;
    },
};

export default objects;
